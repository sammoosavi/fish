set welcome_message ""
set editor "dvim"
set colorscheme "default"

# set new alias
new_alias extree "exa --tree --icons"

# you can add your custom config in here :
